//
//  UIResponder+Extension.swift
//  tip-calculator
//
//  Created by Triandy Gunawan Teng on 13/11/23.
//

import UIKit

extension UIResponder {
    var parentViewController: UIViewController? {
        return next as? UIViewController ?? next?.parentViewController
    }
}
