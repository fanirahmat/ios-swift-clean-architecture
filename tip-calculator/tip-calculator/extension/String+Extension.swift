//
//  String+Extension.swift
//  tip-calculator
//
//  Created by Triandy Gunawan Teng on 20/10/23.
//

import Foundation

extension String {
    var doubleValue: Double? {
        Double(self)
    }
}
