//
//  Int+Extension.swift
//  tip-calculator
//
//  Created by Triandy Gunawan Teng on 17/01/24.
//

import Foundation

extension Int {
    var stringValue: String {
        return String(self)
    }
}
