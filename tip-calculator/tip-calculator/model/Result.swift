//
//  Result.swift
//  tip-calculator
//
//  Created by Triandy Gunawan Teng on 19/10/23.
//

import Foundation

struct Result {
    let amountPerson: Double
    let totalBill: Double
    let totalTip: Double
}
