//
//  ScreenIdentifier.swift
//  tip-calculator
//
//  Created by Mohammad Fani Rahmatulloh on 20/01/24.
//

import Foundation

enum ScreenIdentifier {
    enum ResultView: String {
        case totalAmountPerPersonValueLabel
        case totalBillValueLabel
        case totalTipValueLabel
    }
}
